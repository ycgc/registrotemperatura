/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author ycgc
 */
public class Registry {
    
    private String name;
    private LocalDateTime initialMoment;
    private HashMap<LocalDateTime,Temperature> map;
    private Temperature minimumTemperature;
    private Temperature maximumTemperature;
    
    private static Registry registry;

    /**
     * Initialize a Registry
     * @param name Each registry is identified by a name. Null not allowed. Empty string not allowed
     * @param initialMoment Registry starts recording from this moment. Null not allowed
     * @throws IllegalArgumentException if name is null or empty string
     * @throws IllegalArgumentException if initialMoment is null
     */
    private Registry(String name, LocalDateTime initialMoment) {
        if (name==null) throw new IllegalArgumentException("Name null not allowed");
        if (name.isEmpty()) throw new IllegalArgumentException("Name empty not allowed");
        if (initialMoment==null) throw new IllegalArgumentException("initialMoment null not allowed");
        this.name = name;
        this.initialMoment = initialMoment;
        map = new HashMap<>();
    }
    
    /**
     * Registry is implemented as a Singleton
     * @return the Singleton
     */
    public static Registry getRegistry(){
        if(registry == null){
            registry = new Registry("This is unique!", LocalDateTime.now());
        }   
        return registry;
    }

    /**
     * Name acting as identifier for the Registry
     * @return the name and guarantees not null and not empty
     */
    public String getName() {
        return name;
    }

    /**
     * Registry starts recording at this moment
     * @return the moment and guarantees not null
     */
    public LocalDateTime getInitialMoment() {
        return initialMoment;
    }
     
    protected void setName(String name) {
        this.name = name;
    }

    protected void setInitialMoment(LocalDateTime initialMoment) {
        this.initialMoment = initialMoment;
    }

    /**
     * Collects all temperature objects registered any time.
     * @return a Collection of all temperatures, guarantees not null. 
     */
    public Collection<Temperature> getRegisteredTemperaturesAsCollection() {
        return  map.values();
    }

    /**
     * Obtains a map with the information recorded at the Registry
     * @return  a HashMap, guarantees not null
     */
    public HashMap<LocalDateTime, Temperature> getRegistryAsMap() {
        return (HashMap<LocalDateTime, Temperature>) map.clone();
    }
    
    /**
     * Obtains the temperature that was recorded at the given moment at the Registry
     * @param aMoment that should be indexing a Temperature object
     * @return the temperature indexed by aMoment in the Registry. Guarantees not null
     * @throws IllegalArgumentException aMoment is not a key indexing a Temperature at the Registry
     */
    public Temperature getTemperatureInAMoment(LocalDateTime aMoment) {
        if (!map.containsKey(aMoment)) throw new IllegalArgumentException("Registry doesn't contain a record for this moment.");
        return map.get(aMoment);
    }
    
    /**
     * Obtains the moments where a given Temperature was recorded
     * @param aTemperature given to query the Registry
     * @return always a Set containing the moments indexing the given Temperature. Guarantees not null. 
     * The set is empty if the given temperature was never registered
     */
    public Set<LocalDateTime> getMomentsWithSameTemperature(Temperature aTemperature) {
        Set<LocalDateTime> keys;
        keys = map.keySet();
        for (LocalDateTime k: map.keySet()) {
            if (!map.get(k).equals(aTemperature)) {
                keys.remove(k);
            }
        }
        return keys;
    }
    
    public boolean isEmpty() {
        return map.isEmpty();
    }

    /**
     * Obtains the average of the temperatures in the Registry
     * @return a newly created Temperature object which value corresponds to 
     * the average of the registered temperatures. Guarantees not null.
     * @throws IllegalStateException if the Registry is empty
     */
    public Temperature getAverageTemperature() {
        if (isEmpty()) throw new IllegalStateException("There is no average for an empty Registry.");
       
        Temperature average;    
        float sum = 0;
         
        for (Temperature t: map.values()) {
            sum += t.getValue();
        }
        average = new Temperature(sum/map.size());
        
        return average;
    }

    /**
     * Obtains the minimum of the temperatures in the Registry
     * @return a newly created Temperature object which value corresponds to 
     * the minimum of the registered temperatures. Guarantees not null.
     * @throws IllegalStateException if the Registry is empty
     */
    public Temperature getMinimumTemperature() {
        if (isEmpty()) throw new IllegalStateException("There is no minimum for an empty Registry.");
        return new Temperature(minimumTemperature);
    }

    /**
     * Obtains the maximum of the temperatures in the Registry
     * @return a newly created Temperature object which value corresponds to 
     * the maximum of the registered temperatures. Guarantees not null.
     * @throws IllegalStateException if the Registry is empty
     */
    public Temperature getMaximumTemperature() {
        if (isEmpty()) throw new IllegalStateException("There is no maximum for an empty Registry.");
        return maximumTemperature;
    }
    
    /**
     * Register a temperature object indexed by the moment it was introduced
     * @param aTemperature the temperature object to be registered. Null not allowed
     * @param aMoment the moment when temperature was introduced. Null not allowed
     * @throws IllegalArgumentException when aTemperature is null
     * @throws IllegalArgumentException when aMoment is null
     * 
     */
    protected void addTemperature(Temperature aTemperature, LocalDateTime aMoment){
        if (aTemperature==null) throw new IllegalArgumentException("aTemperature null not allowed");
        if (aMoment==null) throw new IllegalArgumentException("aMoment null not allowed");
        if (map.isEmpty()) {
            minimumTemperature = aTemperature;
            maximumTemperature = aTemperature;
        }
        map.put(aMoment, aTemperature);
        if (aTemperature.compareTo(minimumTemperature)<0) {
            minimumTemperature = aTemperature;
        }
        if (aTemperature.compareTo(maximumTemperature)>0) {
            maximumTemperature = aTemperature;
        }   
    }
    
    /**
     * Create and register a new Temperature object
     * @see Temperature#Temperature(float)
     * @param aValue  a value for initializing the new Temperature object. 
     * @param aMoment Temperature is registered at this moment. Null not allowed
     * 
     */
    public void createNewTemperatureRecord(float aValue, LocalDateTime aMoment){
        if (aMoment==null) throw new IllegalArgumentException("Null not allowed for aMoment.");
        Temperature aTemperature = new Temperature(aValue);
        addTemperature(aTemperature, aMoment);
    } 
    
}
