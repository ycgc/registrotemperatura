/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ycgc
 */
public class Temperature implements Comparable<Temperature> {
    public static final float MINIMUM_ADMISIBLE = -100;
    public static final float MAXIMUM_ADMISIBLE = 100;
    
    private float value;
    
    /**
     * Check whether the given value is correct for a Temperature object
     * @param value should be in range [MINIMUM_ADMISIBLE, MAXIMUM_ADMISIBLE]
     * @return true if value is correct, false in other case
     */
    public static boolean isACorrectValue(float value) {
        return value>=MINIMUM_ADMISIBLE && value<=MAXIMUM_ADMISIBLE;
    }

    /**
     * Initialize a new Temperature object. Check whether value is correct or not.
     * Guarantees {@code new object getValue()==value}
     * @param value should make true {@code isACorrect(value)}
     * @throws IllegalArgumentException if value is not correct
     */
    public Temperature(float value) {
        if (!isACorrectValue(value)) throw new IllegalArgumentException(value + " is not allowed");
        this.value = value;
    }
    
    /** 
     * Copy constructor
     * @param temperature object to copy. Null not allowed
     * @throws IllegalArgumentException if temperature is null
     */
    public Temperature(Temperature temperature) {
       if (temperature==null) throw new IllegalArgumentException("Cannot create a copy from null");
       value = temperature.getValue();
    }

    /**
     * Obtains the numeric value of the temperature object
     * @return a value that guarantees isACorrect(value)
     */
    public float getValue() {
        return value;
    }
    
    @Override
    public String toString() {
        return getValue() + "ºC";
    }
    
    
    /**
     * Compares this object with the specified 'other' object for order.
     * @param other a valid Temperature object
     * @return  a negative integer, zero, or a positive integer as this object is less 
     * than, equal to, or greater than the specified 'other' object. 
     * @throws IllegalArgumentException if the 'other' object is a null reference
     */
    @Override
    public int compareTo(Temperature other) {
        if (other==null) throw new IllegalArgumentException("");
        return Float.compare(getValue(), other.getValue());
    }
    
}
