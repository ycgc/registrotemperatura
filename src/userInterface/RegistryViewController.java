/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface;

import java.time.LocalDateTime;
import model.Registry;
import model.Temperature;

/**
 *
 * @author ycgc
 */
public class RegistryViewController {
    
    private final RegistryView view;
    private float t;
    
    public RegistryViewController(RegistryView view) {
        this.view = view;
    }

    public String getRegistryMaximumTemperature() {
        return Registry.getRegistry().getMaximumTemperature().toString();
    }

    public String getRegistryMinimumTemperature() {
        return Registry.getRegistry().getMinimumTemperature().toString();
    }

    public String getRegistryAverageTemperature() {
        return Registry.getRegistry().getAverageTemperature().toString();
    }
    
    public void processIntroduceTemperature() {
        String temp = view.getTemperatureValue();
        try {
            t = Float.parseFloat(temp);
            if (Temperature.isACorrectValue(t)) {
                view.showRequestConfirmation();
            }
            else { 
                view.showErrorAlert();
            }
        } catch (NumberFormatException e) {
            view.showErrorAlert();
        }
    }

    public void processConfirmTemperatureRegistering() {
        Registry.getRegistry().createNewTemperatureRecord(t, LocalDateTime.now());
        view.closeRequestConfirmation();
        view.showTemperatures(getRegistryAverageTemperature(), getRegistryMinimumTemperature(), getRegistryMaximumTemperature());
    }

    public void processCancelTemperatureRegistering() {
        view.closeRequestConfirmation();
    }

}

