/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface;

import java.awt.Color;

/**
 *
 * @author yania
 */
public class RegistryView extends javax.swing.JFrame {
    
    private static final String NO_VALUE_REGISTERED_YET_MESSAGE = "Nothing registered yet";
    
    private RegistryViewController controller;

    /**
     * Creates new form NewRegistryView
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public RegistryView() {
        initComponents();
        setResizable(false);
        requestConfirmationJPanel.setVisible(false);
        controller = new RegistryViewController(this);
        averageTemperatureValueJLabel.setText(NO_VALUE_REGISTERED_YET_MESSAGE);
        minimumTemperatureValueJLabel.setText(NO_VALUE_REGISTERED_YET_MESSAGE);
        maximumTemperatureValueJLabel.setText(NO_VALUE_REGISTERED_YET_MESSAGE);
    }
    
    public String getTemperatureValue() {
        return newTemperatureValueJTextField.getText();
    }
    
    public void showErrorAlert() {
        newTemperatureValueJTextField.setBackground(Color.RED);
    }

    public void showRequestConfirmation() {
        requestConfirmationJPanel.setVisible(true);
    }

    public void closeRequestConfirmation() {
        requestConfirmationJPanel.setVisible(false);
    }
    
    public void showTemperatures(String average, String minimum, String maximum) {
        temperaturesJPanel.setVisible(true);
        showAverageTemperature(average);     
        showMinimumTemperature(minimum);
        showMaximumTemperature(maximum);
    }
    
    public void showMinimumTemperature(String minimum) {
        if (minimum==null) {
            minimumTemperatureValueJLabel.setText(NO_VALUE_REGISTERED_YET_MESSAGE);
        }
        else {
            minimumTemperatureValueJLabel.setText(minimum);
        }
    }
    
    public void showMaximumTemperature(String maximum) {
        if (maximum==null) {
            maximumTemperatureValueJLabel.setText(NO_VALUE_REGISTERED_YET_MESSAGE);
        }
        else {
            maximumTemperatureValueJLabel.setText(maximum);
        }
    }
    
    public void showAverageTemperature(String average) {
        if (average==null) {
            averageTemperatureValueJLabel.setText(NO_VALUE_REGISTERED_YET_MESSAGE);
        }
        else {
            averageTemperatureValueJLabel.setText(average);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        requestConfirmationJPanel = new javax.swing.JPanel();
        requestConfirmationJLabel = new javax.swing.JLabel();
        confirmJButton = new javax.swing.JButton();
        cancelJButton = new javax.swing.JButton();
        temperaturesJPanel = new javax.swing.JPanel();
        minimunTemperatureTextJLabel = new javax.swing.JLabel();
        minimumTemperatureValueJLabel = new javax.swing.JLabel();
        averageTemperatureTextJLabel = new javax.swing.JLabel();
        averageTemperatureValueJLabel = new javax.swing.JLabel();
        maximumTemperatureTextJLabel = new javax.swing.JLabel();
        maximumTemperatureValueJLabel = new javax.swing.JLabel();
        registerTemperatureJPanel = new javax.swing.JPanel();
        introduceNewValueJLabel = new javax.swing.JLabel();
        newTemperatureValueJTextField = new javax.swing.JTextField();
        registerJButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        requestConfirmationJPanel.setLayout(new javax.swing.BoxLayout(requestConfirmationJPanel, javax.swing.BoxLayout.LINE_AXIS));

        requestConfirmationJLabel.setText("Do you confirm registration of this new value? ");
        requestConfirmationJPanel.add(requestConfirmationJLabel);

        confirmJButton.setText("Yes");
        confirmJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmJButtonActionPerformed(evt);
            }
        });
        requestConfirmationJPanel.add(confirmJButton);

        cancelJButton.setText("No");
        cancelJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelJButtonActionPerformed(evt);
            }
        });
        requestConfirmationJPanel.add(cancelJButton);

        getContentPane().add(requestConfirmationJPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(26, 74, -1, -1));

        temperaturesJPanel.setLayout(new java.awt.GridLayout(3, 2, 2, 0));

        minimunTemperatureTextJLabel.setText("Minimum Temperature:");
        temperaturesJPanel.add(minimunTemperatureTextJLabel);

        minimumTemperatureValueJLabel.setText("jLabel2");
        temperaturesJPanel.add(minimumTemperatureValueJLabel);

        averageTemperatureTextJLabel.setText("Average Temperature:");
        temperaturesJPanel.add(averageTemperatureTextJLabel);

        averageTemperatureValueJLabel.setText("jLabel2");
        temperaturesJPanel.add(averageTemperatureValueJLabel);

        maximumTemperatureTextJLabel.setText("Maximum Temperature:");
        temperaturesJPanel.add(maximumTemperatureTextJLabel);

        maximumTemperatureValueJLabel.setText("jLabel2");
        temperaturesJPanel.add(maximumTemperatureValueJLabel);

        getContentPane().add(temperaturesJPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(108, 125, -1, -1));

        registerTemperatureJPanel.setLayout(new javax.swing.BoxLayout(registerTemperatureJPanel, javax.swing.BoxLayout.LINE_AXIS));

        introduceNewValueJLabel.setText("Introduce new temperature value: ");
        registerTemperatureJPanel.add(introduceNewValueJLabel);

        newTemperatureValueJTextField.setColumns(5);
        newTemperatureValueJTextField.setText("value");
        newTemperatureValueJTextField.setMaximumSize(new java.awt.Dimension(80, 26));
        newTemperatureValueJTextField.setMinimumSize(new java.awt.Dimension(80, 26));
        newTemperatureValueJTextField.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                newTemperatureValueJTextFieldMouseClicked(evt);
            }
        });
        registerTemperatureJPanel.add(newTemperatureValueJTextField);

        registerJButton.setText("Register");
        registerJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerJButtonActionPerformed(evt);
            }
        });
        registerTemperatureJPanel.add(registerJButton);

        getContentPane().add(registerTemperatureJPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(82, 16, -1, 46));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void registerJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerJButtonActionPerformed
        controller.processIntroduceTemperature(); 
    }//GEN-LAST:event_registerJButtonActionPerformed

    private void confirmJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmJButtonActionPerformed
        controller.processConfirmTemperatureRegistering();
    }//GEN-LAST:event_confirmJButtonActionPerformed

    private void cancelJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelJButtonActionPerformed
        controller.processCancelTemperatureRegistering();
    }//GEN-LAST:event_cancelJButtonActionPerformed

    private void newTemperatureValueJTextFieldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newTemperatureValueJTextFieldMouseClicked
        newTemperatureValueJTextField.setText("");
        newTemperatureValueJTextField.setBackground(Color.WHITE);
    }//GEN-LAST:event_newTemperatureValueJTextFieldMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel averageTemperatureTextJLabel;
    private javax.swing.JLabel averageTemperatureValueJLabel;
    private javax.swing.JButton cancelJButton;
    private javax.swing.JButton confirmJButton;
    private javax.swing.JLabel introduceNewValueJLabel;
    private javax.swing.JLabel maximumTemperatureTextJLabel;
    private javax.swing.JLabel maximumTemperatureValueJLabel;
    private javax.swing.JLabel minimumTemperatureValueJLabel;
    private javax.swing.JLabel minimunTemperatureTextJLabel;
    private javax.swing.JTextField newTemperatureValueJTextField;
    private javax.swing.JButton registerJButton;
    private javax.swing.JPanel registerTemperatureJPanel;
    private javax.swing.JLabel requestConfirmationJLabel;
    private javax.swing.JPanel requestConfirmationJPanel;
    private javax.swing.JPanel temperaturesJPanel;
    // End of variables declaration//GEN-END:variables
}
